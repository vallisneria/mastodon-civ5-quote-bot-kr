# 마스토돈 문명 인용구 봇

[![](https://img.shields.io/static/v1?label=Mastodon&message=@civ5_quote_bot@uri.life&color=3088D4&style=flat-square&logo=mastodon)](https://uri.life/@civ5_quote_bot)
[![](https://img.shields.io/static/v1?label=Mastodon&message=@be_quote_bot_kr@uri.life&color=3088D4&style=flat-square&logo=mastodon)](https://uri.life/@be_quote_bot_kr)

2시간 간격으로 문명 시리즈의 인용구를 자동으로 게시합니다.
