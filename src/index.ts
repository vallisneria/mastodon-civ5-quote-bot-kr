import { MastodonClient } from "./mastodon.ts";
import * as quote from "./quote.ts";

const MASTODON_URL = Deno.env.get("MASTODON_URL");
const BEARER_TOKEN = Deno.env.get("BEARER_TOKEN");
const CIV_VERSION = Deno.env.get("CIV_VERSION")?.toLowerCase();

if (MASTODON_URL === undefined) {
  throw new Error("환경 변수 MASTODON_URL이 지정되지 않음.");
}

if (BEARER_TOKEN === undefined) {
  throw new Error("환경 변수 BEARER_TOKEN이 지정되지 않음.");
}

if (
  CIV_VERSION === undefined ||
  !["be", "civ5", "civ6", "works"].includes(CIV_VERSION)
) {
  throw new Error("환경 변수 CIV_VERSION이 지정되지 않음.");
}

let select = await quote.get_random_civ_quote(CIV_VERSION);
let text = quote.to_string(select);

console.log(text + "\n\n");
console.log("업로드 상태 =======================");

let client = new MastodonClient(MASTODON_URL, BEARER_TOKEN);

let img_resp;
if (select._file) {
  let img = new Blob([
    (await Deno.readFile(`./images/${CIV_VERSION}/${select._file}`)).buffer,
  ]);
  img_resp = await client.upload_media(img);

  console.log(`media upload status:   ${img_resp.status}`);
  console.log(`media id:              ${img_resp.id}`);
}

// 툿 업로드
let status_resp;
if (img_resp !== undefined && img_resp.status === 200) {
  status_resp = await client.upload_status(text, "unlisted", "ko", [
    img_resp.id,
  ]);
} else {
  status_resp = await client.upload_status(text, "unlisted", "ko");
}

console.log(`upload status:         ${status_resp.status}`);
console.log(`url:                   ${status_resp.url}`);
