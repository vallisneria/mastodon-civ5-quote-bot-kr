export class MastodonClient {
  base_url: string;
  bearer_token: string;

  constructor(base_url: string, bearer_token: string) {
    const url = new URL(base_url);
    this.base_url = `${url.protocol ?? "https:"}//${url.host}`;
    this.bearer_token = bearer_token;
  }

  async upload_media(file: Blob): Promise<{ status: number; id: string }> {
    const url = new URL("/api/v1/media", this.base_url);
    const form = new FormData();
    form.append("file", file);

    const response = await fetch(url, {
      method: "POST",
      body: form,
      headers: {
        Authorization: `Bearer ${this.bearer_token}`,
      },
    });

    return {
      status: response.status,
      id: (await response.json()).id,
    };
  }

  async upload_status(
    status: string,
    visibility: "public" | "unlisted" | "private" | "direct",
    language: string,
    media_ids?: string[],
  ): Promise<{ status: number; url: string }> {
    const url = new URL("/api/v1/statuses", this.base_url);
    const form = new FormData();
    form.append("status", status);
    form.append("visibility", visibility);
    form.append("language", language);
    media_ids?.forEach((id) => form.append("media_ids[]", id));

    const response = await fetch(url, {
      method: "POST",
      body: form,
      headers: {
        Authorization: `Bearer ${this.bearer_token}`,
      },
    });

    return {
      status: response.status,
      url: (await response.json()).url,
    };
  }
}
