interface CivQuote {
  version: string;
  title?: string;
  quote: string;
  author?: string;
  _file?: string;
}

export async function get_random_civ_quote(version: string): Promise<CivQuote> {
  const civ_quotes = JSON.parse(
    await Deno.readTextFile(`./quotes/${version}.json`),
  );
  return civ_quotes[~~(Math.random() * civ_quotes.length)];
}

export function to_string(selected: CivQuote): string {
  return (
    ("title" in selected ? `[${selected.title}]\n` : "") +
    selected.quote +
    ("author" in selected ? `\n- ${selected.author}` : "")
  );
}
